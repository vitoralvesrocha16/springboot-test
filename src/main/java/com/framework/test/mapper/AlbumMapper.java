package com.framework.test.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.framework.test.dto.AlbumDTO;
import com.framework.test.model.Album;

@Component
public class AlbumMapper {

	public static AlbumDTO toAlbumDTO(Album album) {
		return new AlbumDTO(
				album.getId(),
				album.getTitle(),
				album.getCreatedAt(),
				album.getUpdatedAt(),
				album.getUser().getId());
	}

	public static List<AlbumDTO> toAlbumDTOList(List<Album> albumList) {
		List<AlbumDTO> albumDTOList = new ArrayList<AlbumDTO>();

		albumList.forEach(album -> {
			albumDTOList.add(toAlbumDTO(album));
		});

		return albumDTOList;
	}

	public static Album toAlbum(AlbumDTO albumDTO) {
		return new Album(
				albumDTO.getId(),
				albumDTO.getTitle(),
				albumDTO.getCreatedAt(),
				albumDTO.getUpdatedAt());
	}
}
