package com.framework.test.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.framework.test.dto.UserDTO;
import com.framework.test.model.User;

@Component
public class UserMapper {

	public static UserDTO toUserDTO(User user) {
		return new UserDTO(
				user.getId(),
				user.getFirstName(),
				user.getLastName(),
				user.getEmail(),
				user.getPassword(),
				user.getCreatedAt(),
				user.getUpdatedAt());
	}
	
	public static List<UserDTO> toUserDTOList(List<User> userList) {
		List<UserDTO> userDTOList = new ArrayList<UserDTO>();
		
		userList.forEach(user -> {
			userDTOList.add(toUserDTO(user));
		});
		
		return userDTOList;
	}
	
	public static User toUser(UserDTO userDTO) {
		return new User(
				userDTO.getId(),
				userDTO.getFirstName(),
				userDTO.getLastName(),
				userDTO.getEmail(),
				userDTO.getPassword(),
				userDTO.getCreatedAt(),
				userDTO.getUpdatedAt());
	}
}
