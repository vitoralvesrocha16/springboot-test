package com.framework.test.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.framework.test.dto.PostDTO;
import com.framework.test.model.Post;

@Component
public class PostMapper {

	public static PostDTO toPostDTO(Post post) {
		return new PostDTO(
				post.getId(),
				post.getTitle(),
				post.getContent(),
				post.getCreatedAt(),
				post.getUpdatedAt(),
				post.getUser().getId());
	}

	public static List<PostDTO> toPostDTOList(List<Post> postList) {
		List<PostDTO> postDTOList = new ArrayList<PostDTO>();

		postList.forEach(post -> {
			postDTOList.add(toPostDTO(post));
		});

		return postDTOList;
	}

	public static Post toPost(PostDTO postDTO) {
		return new Post(
				postDTO.getId(),
				postDTO.getTitle(),
				postDTO.getContent(),
				postDTO.getCreatedAt(),
				postDTO.getUpdatedAt());
	}
}
