package com.framework.test.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.framework.test.dto.PhotoDTO;
import com.framework.test.model.Photo;

@Component
public class PhotoMapper {

	public static PhotoDTO toPhotoDTO(Photo photo) {
		return new PhotoDTO(
				photo.getId(),
				photo.getTitle(),
				photo.getUrl(),
				photo.getCreatedAt(),
				photo.getUpdatedAt(),
				photo.getAlbum().getId());
	}

	public static List<PhotoDTO> toPhotoDTOList(List<Photo> photoList) {
		List<PhotoDTO> photoDTOList = new ArrayList<PhotoDTO>();

		photoList.forEach(photo -> {
			photoDTOList.add(toPhotoDTO(photo));
		});

		return photoDTOList;
	}

	public static Photo toPhoto(PhotoDTO photoDTO) {
		return new Photo(
				photoDTO.getId(),
				photoDTO.getTitle(),
				photoDTO.getUrl(),
				photoDTO.getCreatedAt(),
				photoDTO.getUpdatedAt());
	}
}
