package com.framework.test.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.framework.test.dto.CommentDTO;
import com.framework.test.dto.PostDTO;
import com.framework.test.model.Comment;
import com.framework.test.model.Post;

@Component
public class CommentMapper {

	public static CommentDTO toCommentDTO(Comment comment) {
		return new CommentDTO(
				comment.getId(),
				comment.getContent(),
				comment.getCreatedAt(),
				comment.getUpdatedAt(),
				comment.getPost().getId());
	}

	public static List<CommentDTO> toCommentDTOList(List<Comment> commentList) {
		List<CommentDTO> commentDTOList = new ArrayList<CommentDTO>();

		commentList.forEach(comment -> {
			commentDTOList.add(toCommentDTO(comment));
		});

		return commentDTOList;
	}

	public static Comment toComment(CommentDTO commentDTO) {
		return new Comment(
				commentDTO.getId(),
				commentDTO.getContent(),
				commentDTO.getCreatedAt(),
				commentDTO.getUpdatedAt());
	}
}
