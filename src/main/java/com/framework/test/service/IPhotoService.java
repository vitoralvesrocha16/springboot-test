package com.framework.test.service;

import java.util.List;

import com.framework.test.dto.PhotoDTO;

public interface IPhotoService {

	PhotoDTO createPhoto(PhotoDTO photoDTO) throws Exception;
	List<PhotoDTO> getPhotosByAlbum(Long idAlbum) throws Exception;
}
