package com.framework.test.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.test.dto.PhotoDTO;
import com.framework.test.mapper.PhotoMapper;
import com.framework.test.model.Album;
import com.framework.test.model.Photo;
import com.framework.test.repository.IAlbumRepository;
import com.framework.test.repository.IPhotoRepository;

@Service("photoService")
public class PhotoService implements IPhotoService{

	@Autowired
	private final IPhotoRepository photoRepository;

	@Autowired
	private final IAlbumRepository albumRepository;

	@Autowired
	public PhotoService(final IPhotoRepository photoRepository, final IAlbumRepository albumRepository) {
		this.photoRepository = photoRepository;
		this.albumRepository = albumRepository;
	}

	@Override
	public PhotoDTO createPhoto(PhotoDTO photoDTO) throws Exception {
		Optional<Album> optionalAlbum = albumRepository.findById(photoDTO.getAlbum());

		if(optionalAlbum.isPresent()) {
			Photo photo = PhotoMapper.toPhoto(photoDTO);

			Date date = new Date();
			photo.setCreatedAt(date);
			photo.setUpdatedAt(date);
			photo.setAlbum(optionalAlbum.get());

			photoRepository.save(photo);

			return PhotoMapper.toPhotoDTO(photo);
		}

		throw new Exception("Album with id " + photoDTO.getAlbum() + " does not exists!");
	}

	@Override
	public List<PhotoDTO> getPhotosByAlbum(Long idAlbum) throws Exception {
		Optional<Album> optionalAlbum = albumRepository.findById(idAlbum);

		if(optionalAlbum.isPresent()) {
			List<Photo> photos = photoRepository.findByAlbumOrderByUpdatedAtDesc(optionalAlbum.get());

			return PhotoMapper.toPhotoDTOList(photos);
		}

		throw new Exception("Albu with id " + idAlbum + " does not exists!");
	}
}
