package com.framework.test.service;

import java.util.List;

import com.framework.test.dto.AlbumDTO;

public interface IAlbumService {

	AlbumDTO createAlbum(AlbumDTO albumDTO, String email) throws Exception;
	List<AlbumDTO> getAlbumsByUser(Long idUser) throws Exception;
	List<AlbumDTO> getAllAlbums();
}
