package com.framework.test.service;

import java.util.List;

import com.framework.test.dto.UserDTO;

public interface IUserService {

	UserDTO createUser(UserDTO userDTO) throws Exception;
	List<UserDTO> getAllUsers();
}
