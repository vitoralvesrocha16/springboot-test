package com.framework.test.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.framework.test.dto.UserDTO;
import com.framework.test.exception.UserAlreadyExistsException;
import com.framework.test.mapper.UserMapper;
import com.framework.test.model.User;
import com.framework.test.repository.IUserRepository;

@Service("userService")
public class UserService implements IUserService{

	@Autowired
	private final IUserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	public UserService(final IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDTO createUser(UserDTO userDTO) throws Exception {
		User user = userRepository.findByEmail(userDTO.getEmail());

		if(Objects.isNull(user)) {
			user = UserMapper.toUser(userDTO);

			Date date = new Date();
			user.setCreatedAt(date);
			user.setUpdatedAt(date);

			user.setPassword(bcryptEncoder.encode(user.getPassword()));

			userRepository.save(user);

			return UserMapper.toUserDTO(user);
		}

		throw new UserAlreadyExistsException("User already exists!");
	}

	@Override
	public List<UserDTO> getAllUsers() {
		List<User> users = userRepository.findAll();

		return UserMapper.toUserDTOList(users);
	}
}
