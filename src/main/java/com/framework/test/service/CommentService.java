package com.framework.test.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.framework.test.dto.CommentDTO;
import com.framework.test.dto.PostDTO;
import com.framework.test.mapper.CommentMapper;
import com.framework.test.mapper.PostMapper;
import com.framework.test.model.Comment;
import com.framework.test.model.Post;
import com.framework.test.model.User;
import com.framework.test.repository.ICommentRepository;
import com.framework.test.repository.IPostRepository;

@Service("commentService")
public class CommentService implements ICommentService{

	@Autowired
	private final ICommentRepository commentRepository;

	@Autowired
	private final IPostRepository postRepository;

	@Autowired
	public CommentService(final IPostRepository postRepository, final ICommentRepository commentRepository) {
		this.commentRepository = commentRepository;
		this.postRepository = postRepository;
	}

	@Override
	public CommentDTO createComment(CommentDTO commentDTO) throws Exception {
		Optional<Post> optionalPost = postRepository.findById(commentDTO.getPost());

		if(optionalPost.isPresent()) {
			Comment comment = CommentMapper.toComment(commentDTO);

			Date date = new Date();
			comment.setCreatedAt(date);
			comment.setUpdatedAt(date);
			comment.setPost(optionalPost.get());

			commentRepository.save(comment);

			return CommentMapper.toCommentDTO(comment);
		}

		throw new Exception("Post with id " + commentDTO.getPost() + " does not exists!");
	}

	@Override
	public List<CommentDTO> getCommentsByPost(Long idPost) throws Exception {
		Optional<Post> optionalPost = postRepository.findById(idPost);

		if(optionalPost.isPresent()) {
			List<Comment> comments = commentRepository.findByPostOrderByUpdatedAtDesc(optionalPost.get());

			return CommentMapper.toCommentDTOList(comments);
		}

		throw new Exception("Post with id " + idPost + " does not exists!");
	}
}
