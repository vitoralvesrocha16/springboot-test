package com.framework.test.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.framework.test.dto.AlbumDTO;
import com.framework.test.mapper.AlbumMapper;
import com.framework.test.model.Album;
import com.framework.test.model.User;
import com.framework.test.repository.IAlbumRepository;
import com.framework.test.repository.IUserRepository;

@Service("albumService")
public class AlbumService implements IAlbumService{

	@Autowired
	private final IAlbumRepository albumRepository;

	@Autowired
	private final IUserRepository userRepository;

	@Autowired
	public AlbumService(final IAlbumRepository albumRepository, final IUserRepository userRepository) {
		this.userRepository = userRepository;
		this.albumRepository = albumRepository;
	}

	@Override
	public AlbumDTO createAlbum(AlbumDTO albumDTO, String email) throws Exception {
		User user = userRepository.findByEmail(email);

		if(Objects.nonNull(user)) {
			Album album = AlbumMapper.toAlbum(albumDTO);

			Date date = new Date();
			album.setCreatedAt(date);
			album.setUpdatedAt(date);
			album.setUser(user);

			albumRepository.save(album);

			return AlbumMapper.toAlbumDTO(album);
		}

		throw new Exception("User with email " + email + " does not exists!");
	}

	@Override
	public List<AlbumDTO> getAllAlbums() {
		List<Album> albums = albumRepository.findAll(Sort.by(Direction.DESC, "updatedAt"));

		return AlbumMapper.toAlbumDTOList(albums);
	}

	@Override
	public List<AlbumDTO> getAlbumsByUser(Long idUser) throws Exception {
		Optional<User> optionalUser = userRepository.findById(idUser);

		if(optionalUser.isPresent()) {
			List<Album> albums = albumRepository.findByUserOrderByUpdatedAtDesc(optionalUser.get());

			return AlbumMapper.toAlbumDTOList(albums);
		}

		throw new Exception("User with id " + idUser + " does not exists!");
	}
}
