package com.framework.test.service;

import java.util.List;

import com.framework.test.dto.CommentDTO;

public interface ICommentService {

	CommentDTO createComment(CommentDTO commentDTO) throws Exception;
	List<CommentDTO> getCommentsByPost(Long idPost) throws Exception;
}
