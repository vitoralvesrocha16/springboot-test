package com.framework.test.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.framework.test.dto.PostDTO;
import com.framework.test.mapper.PostMapper;
import com.framework.test.model.Post;
import com.framework.test.model.User;
import com.framework.test.repository.IPostRepository;
import com.framework.test.repository.IUserRepository;

@Service("postService")
public class PostService implements IPostService{

	@Autowired
	private final IPostRepository postRepository;

	@Autowired
	private final IUserRepository userRepository;

	@Autowired
	public PostService(final IPostRepository postRepository, final IUserRepository userRepository) {
		this.userRepository = userRepository;
		this.postRepository = postRepository;
	}

	@Override
	public PostDTO createPost(PostDTO postDTO, String email) throws Exception {
		User user = userRepository.findByEmail(email);

		if(Objects.nonNull(user)) {
			Post post = PostMapper.toPost(postDTO);

			Date date = new Date();
			post.setCreatedAt(date);
			post.setUpdatedAt(date);
			post.setUser(user);

			postRepository.save(post);

			return PostMapper.toPostDTO(post);
		}

		throw new Exception("User with email " + email + " does not exists!");
	}

	@Override
	public List<PostDTO> getPostsByUser(Long idUser) throws Exception {
		Optional<User> optionalUser = userRepository.findById(idUser);

		if(optionalUser.isPresent()) {
			List<Post> posts = postRepository.findByUserOrderByUpdatedAtDesc(optionalUser.get());

			return PostMapper.toPostDTOList(posts);
		}

		throw new Exception("User with id " + idUser + " does not exists!");
	}

	@Override
	public List<PostDTO> getAllPosts() {
		List<Post> posts = postRepository.findAll(Sort.by(Direction.DESC, "updatedAt"));

		return PostMapper.toPostDTOList(posts);
	}
}
