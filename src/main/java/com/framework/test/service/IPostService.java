package com.framework.test.service;

import java.util.List;

import com.framework.test.dto.PostDTO;

public interface IPostService {

	PostDTO createPost(PostDTO postDTO, String email) throws Exception;
	List<PostDTO> getPostsByUser(Long idUser) throws Exception;
	List<PostDTO> getAllPosts();
}
