package com.framework.test.dto;

import java.io.Serializable;

public class JwtResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String jwt;

	public JwtResponseDTO(String jwt) {
		this.jwt = jwt;
	}

	public String getToken() {
		return this.jwt;
	}
}
