package com.framework.test.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.framework.test.dto.JwtResponseDTO;
import com.framework.test.dto.UserDTO;
import com.framework.test.exception.UserAlreadyExistsException;
import com.framework.test.service.IUserService;
import com.framework.test.service.JwtUserDetailsService;
import com.framework.test.util.JwtUtil;

@RestController
@CrossOrigin
public class UserController {

	@Autowired
	private IUserService userService;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@GetMapping("/user")
	public List<UserDTO> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/user/signin", method = RequestMethod.POST)
	public ResponseEntity<?> signIn(@RequestBody UserDTO userDTO) throws Exception {
		authenticate(userDTO.getEmail(), userDTO.getPassword());

		//using e-mail as username
		final UserDetails userDetails = userDetailsService.loadUserByUsername(userDTO.getEmail());

		System.out.println(userDetails);
		final String token = jwtUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponseDTO(token));
	}

	@RequestMapping(value = "/user/signup", method = RequestMethod.POST)
	public ResponseEntity<?> singUp(@RequestBody UserDTO userDTO) throws Exception {
		try {
			userDTO = userService.createUser(userDTO);
		} catch (UserAlreadyExistsException e) {
			throw new Exception("USER_ALREADY_EXISTS", e);
		}

		return ResponseEntity.ok(userDTO);
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
