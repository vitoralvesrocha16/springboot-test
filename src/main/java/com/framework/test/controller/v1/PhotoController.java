package com.framework.test.controller.v1;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.framework.test.dto.PhotoDTO;
import com.framework.test.service.IPhotoService;

@RestController
@CrossOrigin
public class PhotoController {

	@Autowired
	private IPhotoService photoService;

	@RequestMapping(value = "/photo", method = RequestMethod.POST)
	public ResponseEntity<?> createPhoto(@Valid @RequestBody PhotoDTO photoDTO) throws Exception {
		return ResponseEntity.ok(photoService.createPhoto(photoDTO));
	}

	@GetMapping("/photo/{album}")
	public List<PhotoDTO> getByAlbum(@PathVariable("album") Long idAlbum) throws Exception {
		return photoService.getPhotosByAlbum(idAlbum);
	}
}
