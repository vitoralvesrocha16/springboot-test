package com.framework.test.controller.v1;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.framework.test.dto.CommentDTO;
import com.framework.test.service.ICommentService;

@RestController
@CrossOrigin
public class CommentController {

	@Autowired
	private ICommentService commentService;

	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	public ResponseEntity<?> createComment(@Valid @RequestBody CommentDTO commentDTO) throws Exception {
		return ResponseEntity.ok(commentService.createComment(commentDTO));
	}

	@GetMapping("/comment/{post}")
	public List<CommentDTO> getByPost(@PathVariable("post") Long idPost) throws Exception {
		return commentService.getCommentsByPost(idPost);
	}
}
