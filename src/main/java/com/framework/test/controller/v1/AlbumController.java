package com.framework.test.controller.v1;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.framework.test.dto.AlbumDTO;
import com.framework.test.dto.PostDTO;
import com.framework.test.service.IAlbumService;

@RestController
@CrossOrigin
public class AlbumController {

	@Autowired
	private IAlbumService albumService;

	@GetMapping("/album/{user}")
	public List<AlbumDTO> getByUser(@PathVariable("user") Long idUser) throws Exception {
		return albumService.getAlbumsByUser(idUser);
	}

	@RequestMapping(value = "/album", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody AlbumDTO albumDTO, Principal principal) throws Exception {

		return ResponseEntity.ok(albumService.createAlbum(albumDTO, principal.getName()));
	}
}
