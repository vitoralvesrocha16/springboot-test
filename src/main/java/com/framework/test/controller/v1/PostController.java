package com.framework.test.controller.v1;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.framework.test.dto.PostDTO;
import com.framework.test.service.IPostService;

@RestController
@CrossOrigin
public class PostController {

	@Autowired
	private IPostService postService;

	@GetMapping("/post/{user}")
	public List<PostDTO> getByUser(@PathVariable("user") Long idUser) throws Exception {
		return postService.getPostsByUser(idUser);
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public ResponseEntity<?> createPost(@Valid @RequestBody PostDTO postDTO, Principal principal) throws Exception {
		return ResponseEntity.ok(postService.createPost(postDTO, principal.getName()));
	}
}
