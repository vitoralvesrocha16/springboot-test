package com.framework.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.test.model.Album;
import com.framework.test.model.Photo;

public interface IPhotoRepository extends JpaRepository<Photo, Long>{

	List<Photo> findByAlbumOrderByUpdatedAtDesc(Album album);
}
