package com.framework.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.test.model.Comment;
import com.framework.test.model.Post;

public interface ICommentRepository extends JpaRepository<Comment, Long>{

	List<Comment> findByPostOrderByUpdatedAtDesc(Post post);
}
