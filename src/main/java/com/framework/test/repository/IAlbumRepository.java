package com.framework.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.test.model.Album;
import com.framework.test.model.User;

public interface IAlbumRepository extends JpaRepository<Album, Long>{

	List<Album> findByUserOrderByUpdatedAtDesc(User user);
}
