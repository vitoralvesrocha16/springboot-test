package com.framework.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.test.model.Post;
import com.framework.test.model.User;

public interface IPostRepository extends JpaRepository<Post, Long>{

	List<Post> findByUserOrderByUpdatedAtDesc(User user);
}
