package com.framework.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.test.model.User;

public interface IUserRepository extends JpaRepository<User, Long>{

	User findByEmail(String email);
}
